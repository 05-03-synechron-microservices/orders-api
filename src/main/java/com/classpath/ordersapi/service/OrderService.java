package com.classpath.ordersapi.service;

import com.classpath.ordersapi.event.EventType;
import com.classpath.ordersapi.event.OrderEvent;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import javax.transaction.Transactional;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final WebClient webClient;
    private final Source source;

    //@CircuitBreaker(name = "inventorymicroservice", fallbackMethod = "fallBack")
    //@Retry(name = "retryConfig", fallbackMethod = "fallBack")
    public Order saveOrder(Order order){
        try {
        //Order savedOrder = this.orderRepository.save(order);
        log.info("Calling the inventory service to update the quantity :: ");
        //ResponseEntity<Integer> integerResponseEntity = this.restTemplate.postForEntity("http://localhost:9222/api/inventory", null, Integer.class);
        /*Integer response = this.webClient
                .post()
                .uri("/api/inventory")
                .retrieve()
                .bodyToMono(Integer.class)
                .block();*/
        //log.info("Response from the inventory microservice :: {}", response);
            MessageBuilder<OrderEvent> orderEvent = MessageBuilder.withPayload(OrderEvent.builder().eventType(EventType.ORDER_ACCEPTED).order(order).build();
            this.source.output()
                    .send(orderEvent.build());
        }catch (Exception exception){
            log.error("Exception while pushing the message to kafka-topic :: {}", exception.getMessage());
            exception.printStackTrace();
        }
        return Order.builder().build();
    }

    private Order fallBack(Throwable exception){
        log.error("Exception while invoking the REST endpoint :: ", exception.getMessage());
        return Order.builder().build();
    }

    public Map<String, Object> fetchOrders(int pageNo, int noOfElements, String order, String property){
        log.info("INside the fetch Orders method: {}, {}, {}, {}", pageNo, noOfElements, order, property);
        //pagination
        var direction = order.equalsIgnoreCase("ASC") ? Sort.Direction.ASC: Sort.Direction.DESC;
        var pageRequest = PageRequest.of(pageNo, noOfElements, direction, property);
        Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        long totalElements = pageResponse.getTotalElements();
        int totalPages = pageResponse.getTotalPages();
        List<Order> orders = pageResponse.getContent();
        int number = pageResponse.getNumber();

        var responseMap = new LinkedHashMap<String, Object>();
        responseMap.put("total", totalElements);
        responseMap.put("size", noOfElements);
        responseMap.put("pages", totalPages);
        responseMap.put("current", number);
        responseMap.put("data", orders);
        return responseMap;
    }

    public Order findOrderById(long orderId){
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("invalid orderid"));
    }

    @Transactional
    public void deleteOrderById(long orderId){
        log.info(" Came inside the delete order method :: {}",orderId);
        this.orderRepository.deleteById(orderId);

    }

    public Set<Order> fetchOrdersByPriceBetween(double min, double max){
        return new HashSet<>(this.orderRepository.findByPriceBetween(min, max));
    }
}
