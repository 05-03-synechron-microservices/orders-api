package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.User;
import com.classpath.ordersapi.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
@Slf4j
public class DomainUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Came inside the loadUserByUsername method :: {}", username);
        return this.userRepository
                .findByUserName(username)
                .orElseThrow(() -> new UsernameNotFoundException("Invalid user"));
    }
}
