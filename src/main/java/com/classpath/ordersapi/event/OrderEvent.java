package com.classpath.ordersapi.event;

import com.classpath.ordersapi.model.Order;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import java.time.LocalDateTime;

@Data
@Builder
@AllArgsConstructor
public class OrderEvent {
    private final Order order;
    private final EventType eventType;
    private final LocalDateTime timestamp = LocalDateTime.now();
}
