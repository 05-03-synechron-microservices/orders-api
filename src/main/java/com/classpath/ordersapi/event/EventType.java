package com.classpath.ordersapi.event;

public enum EventType {
    ORDER_ACCEPTED,
    ORDER_PENDING,
    ORDER_FULFILLED,
}
