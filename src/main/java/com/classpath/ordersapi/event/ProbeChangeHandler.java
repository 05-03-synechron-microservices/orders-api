
package com.classpath.ordersapi.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
class LivenessProbeChangeHandler {

    @EventListener
    public void onLivenessProbeEvent(AvailabilityChangeEvent<LivenessState> event){
      log.info("Status of current liveness probe:: State: {} message :: {} ", event.getState(), event.getSource());
    }
}

@Component
@Slf4j
class ReadinessProbeChangeHandler {

    @EventListener
    public void onReadinessProbeEvent(AvailabilityChangeEvent<ReadinessState> event){
        log.info("Status of current Readiness probe:: State: {} message :: {} ", event.getState(), event.getSource());
    }
}
