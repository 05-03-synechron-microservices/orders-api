package com.classpath.ordersapi.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;
import java.util.List;

@Configuration
public class ApplicationConfiguration implements CommandLineRunner {

    private final ApplicationContext applicationContext;

    public ApplicationConfiguration(ApplicationContext applicationContext){
        this.applicationContext = applicationContext;
    }


    @Override
    public void run(String... args) throws Exception {
        System.out.println("=============================");
        //List<String> beanDefinitionNames = List.of(this.applicationContext.getBeanDefinitionNames());
        List<String> beanDefinitionNames = Arrays.asList(applicationContext.getBeanDefinitionNames());
        beanDefinitionNames.stream()
                            .filter(beanName -> beanName.startsWith("loadUser"))
                            .forEach(beanName -> System.out.println("Bean name: "+ beanName));
        System.out.println("=============================");
    }
}
