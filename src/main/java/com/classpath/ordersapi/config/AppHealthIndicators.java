package com.classpath.ordersapi.config;

import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator{

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        try {
            this.orderRepository.count();
            return Health.up().withDetail("DB", "DB service is up").build();
        } catch (Exception exception) {
            return Health.down().withDetail("DB", "DB is unreachable").build();
        }
    }
}
