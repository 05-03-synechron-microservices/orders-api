package com.classpath.ordersapi.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@Slf4j
public class WebApplicationConfiguration implements WebMvcConfigurer {

   /* @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        log.info("Came inside the content negotiation method :: ");
        configurer
                .defaultContentType(MediaType.APPLICATION_XML);
    }
*/
    @Override
    public void addCorsMappings(CorsRegistry registry) {

        registry.addMapping("/api/v1/orders").allowedOrigins("&");
    }


}
