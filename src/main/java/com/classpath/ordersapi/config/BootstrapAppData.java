package com.classpath.ordersapi.config;

import com.classpath.ordersapi.model.LineItem;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.model.Role;
import com.classpath.ordersapi.model.User;
import com.classpath.ordersapi.repository.OrderRepository;
import com.classpath.ordersapi.repository.UserRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.EventListener;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.transaction.Transactional;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import static java.util.stream.IntStream.range;

@Configuration
@RequiredArgsConstructor
//@Profile(value = {"dev", "qa"})
public class BootstrapAppData {

    private final OrderRepository orderRepository;
    private final UserRepository userRepository;
    private final Faker faker = new Faker();

    @Value("${app.data.limit}")
    private int limit;


    @EventListener(ApplicationReadyEvent.class)
    @Transactional
    public void onApplicationReady(ApplicationReadyEvent event){
        System.out.println("============ Application initializing ==================");
        List<Order> orders = new ArrayList<>();

        Role userRole = Role.builder().roleName("USER").build();
        Role adminRole = Role.builder().roleName("ADMIN").build();
        Role superAdminRole = Role.builder().roleName("SUPER_ADMIN").build();
        range(0, limit).forEach(index -> {
            Order order = Order
                            .builder()
                                .orderDate(faker.date().past(7, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                .price(faker.number().randomDouble(2, 15_000, 35_000))
                                .build();
            range(0, faker.number().numberBetween(2,4)).forEach(value -> {
                LineItem lineItem = LineItem.builder()
                                        .name(faker.commerce().productName())
                                        .price(faker.number().randomDouble(2, 2000, 5000))
                                        .qty(faker.number().numberBetween(2, 6))
                                        .build();
               order.addLineItem(lineItem);
               orders.add(order);
            });
        });

        //Assing orders to each of the 5 employees
        List<User> users = new ArrayList<>();
        range(0, 15).forEach(customerIndex -> {
            User user = User.builder()
                                .userName(faker.name().firstName())
                                .accountNonExpired(true)
                                .credentialsNonExpired(true)
                                .accountNonLocked(true)
                                .enabled(true)
                                //.password("welcome")
                                .password(new BCryptPasswordEncoder().encode("welcome"))
                             .build();
            users.add(user);
        });

        range(0, 11).forEach(customerIndex -> {
            for( int orderId = 0; orderId < 25; orderId++){
                User customer = users.get(customerIndex);
                customer.addOrder(orders.get(orderId));
            }
            if(customerIndex % 2 == 0){
                users.get(customerIndex).addUser(userRole);
            } else if (customerIndex % 5 == 0){
                User currentUser = users.get(customerIndex);
                currentUser.addUser(adminRole);
                currentUser.addUser(userRole);
                currentUser.addUser(superAdminRole);
            } else if (customerIndex % 2 != 0) {
                User currentUser = users.get(customerIndex);
                currentUser.addUser(adminRole);
                currentUser.addUser(userRole);
            }
            this.userRepository.save(users.get(customerIndex));
        });
    }
}
