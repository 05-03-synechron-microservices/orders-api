package com.classpath.ordersapi.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(NOT_FOUND)
    public Error handleInvalidOrderId(IllegalArgumentException exception){
        log.error(" Exception while fetching the order id which is invalid: {}", exception.getMessage());
        return new Error(exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(BAD_REQUEST)
    public Set<String> handleInvalidMessage(MethodArgumentNotValidException exception){
       log.error(" Exception while fetching the order id which is invalid: {}", exception.getMessage());
        List<ObjectError> allErrors = exception.getAllErrors();
        return allErrors.stream().map(error -> error.getDefaultMessage()).collect(Collectors.toSet());
    }
}

@AllArgsConstructor
@Getter
class Error {
    private String message;
}
