
package com.classpath.ordersapi.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.availability.ApplicationAvailability;
import org.springframework.boot.availability.AvailabilityChangeEvent;
import org.springframework.boot.availability.LivenessState;
import org.springframework.boot.availability.ReadinessState;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/api/health")
@RequiredArgsConstructor
@Slf4j
public class ApplicationProbeController {

    private final ApplicationAvailability applicationAvailability;
    private final ApplicationEventPublisher applicationEventPublisher;

    @PostMapping("/liveness")
    public Map<String, Object> updateLivessState(){
        LivenessState currentLivenessState = this.applicationAvailability.getLivenessState();
        LivenessState updatedLivenessState = currentLivenessState == LivenessState.CORRECT ? LivenessState.BROKEN : LivenessState.CORRECT;
        String status = updatedLivenessState == LivenessState.CORRECT ? "Application is functional " : "Application is NON-Functional";
        LinkedHashMap<String, Object> response = new LinkedHashMap<>();
        response.put("state", updatedLivenessState);
        response.put("message", status);
        AvailabilityChangeEvent.publish(applicationEventPublisher, status, updatedLivenessState);
        return response;
    }
    @PostMapping("/readiness")
    public Map<String, Object> updateReadinessState(){
        ReadinessState currentReadinessState = this.applicationAvailability.getReadinessState();
        ReadinessState updatedReadinessState = currentReadinessState == ReadinessState.ACCEPTING_TRAFFIC ? ReadinessState.REFUSING_TRAFFIC : ReadinessState.ACCEPTING_TRAFFIC;
        String status = updatedReadinessState == ReadinessState.ACCEPTING_TRAFFIC ? "Application is functional " : "Application is NON-Functional";
        LinkedHashMap<String, Object> response = new LinkedHashMap<>();
        response.put("state", updatedReadinessState);
        response.put("message", status);
        AvailabilityChangeEvent.publish(applicationEventPublisher, status, updatedReadinessState);
        return response;
    }
}
