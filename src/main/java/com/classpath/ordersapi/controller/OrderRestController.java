package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.PastOrPresent;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.MediaType.APPLICATION_XML_VALUE;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @GetMapping
    @Operation(method = "fetchOrders", description = "Fetching all the orders")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",description = "Sucessfull orders"),
            @ApiResponse(responseCode = "401",description = "Incorrect credentials"),
            @ApiResponse(responseCode = "403",description = "Access denied")
    })
    public Map<String, Object> fetchOrders(
            @RequestParam(value = "pageNo", defaultValue = "0",required = false )
            @Parameter(name = "pageNo", required = false, allowEmptyValue = true) int pageNo,
            @RequestParam(value = "size", defaultValue = "10", required = false)
            @Parameter(name = "size", required = false, allowEmptyValue = true) int size,
            @RequestParam(value = "order", defaultValue = "ASC", required = false)
            @Parameter(name = "order", required = false, allowEmptyValue = true) String order,
            @RequestParam(value = "field", defaultValue = "id", required = false)
            @Parameter(name = "property", required = false, allowEmptyValue = true) String property){
        return this.orderService.fetchOrders(pageNo, size, order, property);
    }

    @GetMapping("/price")
    public Set<Order> fetchOrders(
            @RequestParam(value = "min", defaultValue = "10000",required = false ) double min,
            @RequestParam(value = "max", defaultValue = "25000", required = false) double max){
        return this.orderService.fetchOrdersByPriceBetween(min, max);
    }

    @GetMapping(value = "/{id}")
    public Order fetchOrderById(@PathVariable long id){
        return this.orderService.findOrderById(id);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    public Order saveOrder(@RequestBody @Valid Order order){
        return this.orderService.saveOrder(order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    public void deleteOrderById(@PathVariable long id){
        this.orderService.deleteOrderById(id);
    }
}
