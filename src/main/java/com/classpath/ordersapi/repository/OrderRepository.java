package com.classpath.ordersapi.repository;

import com.classpath.ordersapi.model.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    List<Order> findByPriceBetween(double min, double max);
    Page<Order> findByOrderDateBetween(LocalDate start, LocalDate endDate, PageRequest pageRequest);
}
