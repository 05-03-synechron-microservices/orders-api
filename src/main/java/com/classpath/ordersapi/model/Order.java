package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import static javax.persistence.GenerationType.AUTO;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "orders")
@EqualsAndHashCode(exclude = "user")
@ToString(exclude = "user")
public class Order {
    @Id
    @GeneratedValue(strategy = AUTO)
    private Long id;

    @PastOrPresent(message = "order date cannot be in the future")
    private LocalDate orderDate;

    @Min(value = 10000, message = "order price cannot be less than 10,000")
    @Max(value = 50000, message = "order price cannot be more than 50,000")
    private double price;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonManagedReference
    @Valid
    private Set<LineItem> lineItems;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    @JsonIgnore
    private User user;

    public void addLineItem(LineItem lineItem){
        if(this.lineItems == null){
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}
