package com.classpath.ordersapi.util;


import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.autoconfigure.data.ConditionalOnRepositoryType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

class User {

}

@Configuration
public class BeanDefinitions {

    @Bean
    @ConditionalOnProperty(prefix = "app", name = "loadUserBean", havingValue = "true", matchIfMissing = true)
    public User loadUserBeanBasedOnProperty() {
        return new User();
    }

    @Bean
    @ConditionalOnBean(name = "loadUserBeanBasedOnProperty")
    public User loadUserBeanBasedOnBean() {
        return new User();
    }
    @Bean
    @ConditionalOnMissingBean(name = "loadUserBeanBasedOnProperty")
    public User loadUserBeanBasedOnMissingBean() {
        return new User();
    }

    @Bean
    @ConditionalOnClass(name = "com.classpath.ordersapi.model.Order")
    public User loadUserBeanBasedOnClass() {
        return new User();
    }
    @Bean
    @ConditionalOnMissingClass(value = "com.classpath.ordersapi.model.Order")
    public User loadUserBeanBasedOnMissingClass() {
        return new User();
    }
}
