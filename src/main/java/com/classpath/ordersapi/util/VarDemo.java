package com.classpath.ordersapi.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class VarDemo {
    public static void main(String[] args) {
        var values = Set.of("one", "two", "three", "four", "five");
        Map<Integer,Set<String>> result = new HashMap<>();
        result.put(1, values);
        result.put(2, values);

        var entries = result.entrySet();
        var iterator = entries.iterator();
        while(iterator.hasNext()){
            var next = iterator.next();
            System.out.print("Key :: "+ next.getKey());
            System.out.print("Value :: "+ next.getValue());
        }
    }
}
