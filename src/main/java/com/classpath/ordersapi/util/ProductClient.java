package com.classpath.ordersapi.util;


import com.github.javafaker.Faker;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
class Product {
    private long id;
    private String name;
    private double price;
    private Company company;
}

enum Company {
    SAMSUNG,
    APPLE,
    LG,
    LENOVO
}
public class ProductClient {
    private static Faker faker = new Faker();

    public static void main(String[] args) {
        List<Product> products = fetchProducts();
        Predicate<Product> isIPhone = product -> product.getName().startsWith("IPhone");
        Predicate<Product> isMacBookPro = product -> product.getName().startsWith("Mac-Book-Pro");
        Predicate<Product> isAppleProduct = isIPhone.or(isMacBookPro);
        Predicate<Product> isNotAppleProduct = isAppleProduct.negate();
        Predicate<Product> productPriceBetween20And50K = product -> product.getPrice() < 20000 && product.getPrice() > 50000;
        filterProducts(products, isIPhone);

    }
    //strategy
    public static List<Product> filterProducts(List<Product> products, Predicate<Product> predicate){
        return products.stream().filter(predicate).collect(Collectors.toList());
    }

    private static List<Product> fetchProducts (){
        List<Product> products = new ArrayList<>();
        IntStream.range(0, 10).forEach(index -> {
            Product product = Product.builder().company(Company.APPLE).name("IPhone-12").price(faker.number().randomDouble(2, 50_000, 65_000)).build();
            products.add(product);
        });
        IntStream.range(0, 10).forEach(index -> {
            Product product = Product.builder().company(Company.APPLE).name("Mac-Book-Pro").price(faker.number().randomDouble(2, 1_50_000, 2_65_000)).build();
            products.add(product);
        });
        IntStream.range(0, 10).forEach(index -> {
            Product product = Product.builder().company(Company.SAMSUNG).name("Galaxy-S12").price(faker.number().randomDouble(2, 50_000, 85_000)).build();
            products.add(product);
        });
        IntStream.range(0, 10).forEach(index -> {
            Product product = Product.builder().company(Company.SAMSUNG).name("Galaxy-S11").price(faker.number().randomDouble(2, 40_000, 45_000)).build();
            products.add(product);
        });
        IntStream.range(0, 10).forEach(index -> {
            Product product = Product.builder().company(Company.LG).name("LG-phone").price(faker.number().randomDouble(2, 10_000, 55_000)).build();
            products.add(product);
        });
        return  products;
    }
}
