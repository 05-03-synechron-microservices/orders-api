package com.classpath.ordersapi.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

public class BcryptPasswordEncoderDemo {

    public static void main(String[] args) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String rawText = "welcome";
        String encodedPassword1 = passwordEncoder.encode(rawText);
        String encodedPassword2 = passwordEncoder.encode(rawText)+ "%";
        String encodedPassword3 = passwordEncoder.encode(rawText);
        String encodedPassword4 = passwordEncoder.encode(rawText);

        List.of(encodedPassword1, encodedPassword2, encodedPassword3, encodedPassword4)
                .stream().filter(encodedPassword -> passwordEncoder.matches(rawText, encodedPassword))
                .forEach(System.out::println);



    }
}
