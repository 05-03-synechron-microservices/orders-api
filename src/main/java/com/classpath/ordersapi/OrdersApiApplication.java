package com.classpath.ordersapi;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@OpenAPIDefinition( info = @Info(
								title = "Orders API",
								version = "1.0.0",
								contact = @Contact(email = "developer@classpath.io")),
					tags = @Tag(name="orders-microservice"))
@EnableWebSecurity
@EnableBinding(Source.class)
public class OrdersApiApplication {
	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(OrdersApiApplication.class);
		application.setApplicationStartup(new BufferingApplicationStartup(1500));
		application.run(args);
	}
}
